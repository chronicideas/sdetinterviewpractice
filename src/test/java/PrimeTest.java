import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PrimeTest {

    private Prime prime = new Prime();

    @Test
    void testCountNumberOfPrimesUpTo() {
        assertEquals(4, prime.countNumberOfPrimesUpTo(10));
    }
}