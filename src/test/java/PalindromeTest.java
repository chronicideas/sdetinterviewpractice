import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PalindromeTest {

    private Palindrome palindrome = new Palindrome();

    @Test
    void testLongestPalindromeInString_CorrectlyFound() {
        assertEquals("noon", palindrome.getLongestPalindromeInString("The sun at noon is the highest!"));
    }
}