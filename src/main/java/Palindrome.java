class Palindrome {

    String getLongestPalindromeInString(String string) {
        String longest = string.substring(0,1);
        for (int i = 0; i < string.length() - 1; i++) {
            String palindrome = intermediatePalindrome(string, i, i); //odd cases
            if (palindrome.length() > longest.length()) {
                longest = palindrome;
            }

            palindrome = intermediatePalindrome(string, i, i+1);
            if (palindrome.length() > longest.length()) {
                longest = palindrome;
            }
        }
        return longest.trim();
    }

    private String intermediatePalindrome(String string, int left, int right) {
        if (left > right) return null;
        while (left >= 0 && right < string.length() && string.charAt(left) == string.charAt(right)) {
            left--;
            right++;
        }
        return string.substring(left + 1, right);
    }
}
