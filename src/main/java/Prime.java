public class Prime {

    public int countNumberOfPrimesUpTo(int number) {
        int count = 0;
        for (int i = 1; i <= number; i++) {
            if (isPrime(i)) {
                System.out.println(i);
                count++;
            }
        }
        return count;
    }

    private boolean isPrime(int number) {
        if (number < 2) return false;
        else if (number < 3) return true;

        for (int i = 2; i < Math.sqrt(number) + 1; i++) {
            if (number % i == 0) return false;
        }
        return true;
    }
}
